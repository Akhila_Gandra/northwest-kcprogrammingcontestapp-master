//
//  StudentsViewController.swift
//  Northwest-KCProgrammingContestApp
//
//  Created by Akhila Gandra on 3/13/19.
//  Copyright © 2019 Akhila Gandra . All rights reserved.
//

import UIKit

class StudentsViewController: UIViewController {

    @IBOutlet weak var studentLBL: UILabel!
    @IBOutlet weak var student1LBL: UILabel!
    @IBOutlet weak var student2LBL: UILabel!
    var team: Team!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        studentLBL.text = team.students[0]
        student1LBL.text = team.students[1]
        student2LBL.text = team.students[2]
        navigationItem.title = team.name
    }
    
}
