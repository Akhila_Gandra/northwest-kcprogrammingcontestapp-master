//
//  NewTeamViewController.swift
//  Northwest-KCProgrammingContestApp
//
//  Created by Akhila Gandra  on 3/13/19.
//  Copyright © 2019 Akhila Gandra  . All rights reserved.
//

import UIKit

class NewTeamViewController: UIViewController {

    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var STU0: UITextField!
    @IBOutlet weak var STU1: UITextField!
    @IBOutlet weak var STU2: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    var school: School!
    @IBAction func done(_ sender: Any) {
        let name = nameTF.text!
        let stu0Name = STU0.text!
        let stu1Name = STU1.text!
        let stu2Name = STU2.text!
        school.addTeam(name: name, students: [stu0Name, stu1Name, stu2Name])
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
  
}
